from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
import os
import argparse

parser = argparse.ArgumentParser(description="Performs searches against google.ca and check if the first search result leads to a domain belonging to '*.finn.ai'")	
parser.add_argument('-f', dest='filename', required=False, metavar="FILE", help='input file containing user defined search items, file format: one line one search term')				
args = parser.parse_args()

dt_string = datetime.now().strftime("%Y-%m-%d-%H%M%S")

def search_finn_ai(search_terms, output_file):
	for search_term in search_terms:	
		#go to google's website
		driver.get("http://www.google.ca")
		assert "Google" in driver.title
		
		#search 
		elem = driver.find_element(By.NAME, "q")
		elem.clear()
		elem.send_keys(search_term)
		elem.send_keys(Keys.RETURN)
		
		#check the first search result (ignore ads)
		WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'div#search')))
		first_link = driver.find_element(By.XPATH, "(//div[@id='search']//*[@href])[1]").get_attribute('href')
		if ".finn.ai" in first_link:
			output_file.write(f"{search_term}: Pass \r\n")
		else:
			#take screenshots for failures
			driver.get_screenshot_as_file(f"output\{dt_string}\{search_term}.png")			
			output_file.write(f"{search_term}: Fail \r\n")

	

if __name__ == '__main__':
	driver = webdriver.Chrome("chromedriver_win32\chromedriver")
	driver.maximize_window()
	
	search_terms = ["Finn AI", "Personal banking chatbot", "Natural language banking chatbot", "AI powered banking chatbot", "AI platform for banking"]
	#attend default search list if there's user input file in command
	if args.filename != None:
		with open(args.filename, 'r') as file:
			rows = file.readlines()
			user_list = [row.strip() for row in rows]		
			search_terms = [*search_terms, *user_list]
	
	#create output folder and output file
	filePath = f"output\{dt_string}"	
	if not os.path.exists(filePath):
		os.makedirs(filePath)
	result_file= open(f"{filePath}\seleniumTestOutput.txt","w+")
	print(f"Output file path: '{filePath}\seleniumTestOutput.txt'")
	
	search_finn_ai(search_terms, result_file)
		
	result_file.close()
			
	driver.close()
